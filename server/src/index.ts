/**
 * 
 *  JOGO POKER (No-limit Texas Hold'em) - Estilo torneio
 * 
 * 1 - Criar jogo/mesa
 * 2 - Cadastrar jogadores na mesa
 * 3 - iniciar jogo (ter ao menos 2 jogadores)
 * 4 - distribuir cartas
 * 5 - receber jogadas
 * 6 - distribuir flop, turn, river
 * 7 - redistribuir pot após a mão
 * 8 - jogo termina quando somente um jogador tem fichas
 * 
 * Autor: Caio Sobreiro dos Santos (caiosobreiro@gmail.com)
 */

import { Player } from "./Player"
import { Table } from "./Table"
import { createServer } from "http"
import { Server, Socket } from "socket.io"
import { v4 } from 'uuid'
import express from 'express'
import cors from 'cors'

const app = express()

app.use(cors())

const httpServer = createServer()
const io = new Server(httpServer, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
    }
})

httpServer.listen(7071)
app.listen(7070, () => {
    console.log('Listening on port 7070')
})
console.log('Socket server listening on port 7071')

let tables: Table[] = [];

io.on('connection', (socket: Socket) => {

    const id = v4()
    console.log(`Client connected with id ${id}`)

    socket.on('message', async (messageBuffer) => {
        const [command, name, tableId] = messageBuffer.toString().trim().split(' ')
        if (command === 'join') {
            console.log(`Adding player ${id} ${name}`)
            const player = new Player(name, socket)
            const table = tables.find(table => table.getId() === tableId)
            if (!table) {
                console.error(`Table with id ${tableId} not found`)
            }
            table.setPlayer(player)
            socket.send(JSON.stringify({ message: 'Joined table: ' + table.getId(), playerName: name }))
        }
    })

    socket.on("close", () => {
        console.log(`Player ${id} has lost connection`)
        // if (table) table.playerDisconnect(socket)
    })
})

app.get('/', function (req, res) {
    res.send({ version: '1.0.0' })
})

// create table
app.post('/tables', function (req, res) {
    console.log('Creating game...')
    const table = new Table()
    tables.push(table)
    res.send(tables.map(table => ({ id: table.getId(), name: 'Replace with random' })))
})

// list tables
app.get('/tables', function (req, res) {
    // if (!table) return console.error('No table created')
    // console.log(table.getPlayers().map(p => p.getName()).join(', '))
    res.send(tables.map(table => ({ id: table.getId(), name: 'Replace with random' })))
})

app.delete('/tables/:id', function (req, res) {
    const { id } = req.params
    const index = tables.indexOf(tables.find(table => table.getId() === id))
    if (index === -1) {
        console.error(`Table ${id} not found`)
        return res.sendStatus(404)
    }
    tables.splice(index, 1)
    res.send(tables.map(table => ({ id: table.getId(), name: 'Replace with random' })))
})

// join
// app.post('/table/:user', function (req, res) {
//     const { user } = req.params
//     console.log(`Adding player ${user}`)
//     const player = new Player(user)
//     table.setPlayer(player)
//     res.send({ message: 'Joined table: ' + table.getId(), playerName: user })
// })

// start game
app.post('/start/:tableId', function (req, res) {
    console.log('Game is starting')
    const { tableId } = req.params
    const table = tables.find(table => table.getId() === tableId)
    table.broadcastMessage({ message: 'Game is starting', numberOfPlayers: table.getPlayers().length, tableId: table.getId() })
    table.game.runGame()
    res.send({ message: 'started', id: table.getId() })
})

// quit
// app.delete


