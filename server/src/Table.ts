import { Game } from "./Game"
import { Player } from "./Player"
import * as _ from 'lodash'
import { v4 } from 'uuid'
import { Socket } from "socket.io"

export class Table {
    private id: string
    private players: Player[]
    private dealerPosition: number
    game: Game

    constructor() {
        this.id = v4().substr(0, 6)
        this.players = []
    }

    getPlayers () {
        return this.players
    }

    getId () {
        return this.id
    }

    setPlayer (player: Player) {
        player.setChips(1500)
        this.players.push(player)
        this.players = _.shuffle(this.players)
        this.dealerPosition = _.random(this.players.length - 1)
    }

    removePlayer (player: Player) {
        this.players.splice(this.players.indexOf(player), 1)
    }

    playerDisconnect (ws: Socket) {
        this.players.splice(this.players.indexOf(this.players.find(p => p.ws === ws)), 1)
        if (!this.players.length) {
            this.game = null
        }
    }

    startGame () { // [1, 2, 3]
        this.dealerPosition = this.dealerPosition + 1 > this.players.length - 1 ? 0 : this.dealerPosition + 1
        const game = new Game(this.players, this.dealerPosition, this)
        this.game = game
        return game
    }

    broadcastMessage(message: string | object) {
        let obj = {} as any
        if (typeof message === 'string') {
            obj.message = message
        } else {
            obj = message
        }
        this.players.forEach(player => {
            player.ws.send(JSON.stringify(obj))
        })
    }
}