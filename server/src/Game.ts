import { Card, Deck } from "./Deck"
import { Player } from "./Player"
import * as _ from 'lodash'
import * as helpers from './Helpers'
import { Table } from "./Table"

export class Game {
    private players: Player[]
    private deck: Deck
    private pot: number = 0
    private openCards = []
    private dealerPosition: number
    private table: Table

    private smallBlind = 100
    private bigBlind = 200

    private biggestBet = this.bigBlind
    private biggestBetPlayer

    constructor (players: Player[], dealerPosition: number, table: Table) {
        this.deck = new Deck()
        this.players = players
        this.dealerPosition = dealerPosition
        this.table = table
    }

    givePlayerCards () {
        this.players.forEach(player => {
            const hand = [this.deck.draw(), this.deck.draw()]
            player.setHand(hand)
        })
    }

    setBlinds() {
        const sb = this.getPlayerAtPositionRelativeToDealer(1)
        const bb = this.getPlayerAtPositionRelativeToDealer(2)
        const dealer = this.getPlayerAtPositionRelativeToDealer(0)
        this.makeBet(sb, this.smallBlind)
        this.makeBet(bb, this.bigBlind)
        console.log(`SB: ${sb.getName()} BB: ${bb.getName()} Dealer: ${dealer.getName()}`)
    }

    getPot () {
        return this.pot
    }

    cashPot (player: Player) {
        player.setChips(player.getChips() + this.pot)
        this.pot = 0
    }

    makeBet (player: Player, value: number) {
        const playerBet = player.getBet() + value
        player.makeBet(value)
        this.pot += value
        if (this.biggestBet < playerBet) {
            this.biggestBet = playerBet
            this.biggestBetPlayer = player
        }
    }

    getPlayerToPlay () {
        // If its the first move, UTG to play
        if (this.pot === this.smallBlind + this.bigBlind) {
            return this.getPlayerAtPositionRelativeToDealer(3)
        }
        return this.getPlayerAtPositionRelativeToDealer(1)
    }

    private getPlayerAtPositionRelativeToDealer (offset: number) {
        const playersToPickFrom = this.players.concat(this.players)
        return playersToPickFrom[this.dealerPosition + offset]
    }

    setPlayOrder (preFlop: boolean) {
        helpers.arrayRotate(this.players, preFlop ? this.dealerPosition + 3 : this.dealerPosition + 1)
    }

    getPossibleMoves(player: Player) {
        const currentBet = player.getBet()
        console.log('currentBet', currentBet, 'biggestBet', this.biggestBet)
        if (currentBet < this.biggestBet) {
            return ['call', 'raise', 'fold']
        }
        return ['check', 'raise']
    }

    getPlayers () {
        return this.players
    }

    getBiggestBet () {
        return this.biggestBet
    }

    setBiggestBet (value: number) {
        this.biggestBet = value
    }

    getBiggestBetPlayer () {
        return this.biggestBetPlayer
    }

    setBiggestBetPlayer (value: number) {
        this.biggestBetPlayer = value
    }

    removePlayer (player: Player) {
        this.players.splice(this.players.indexOf(player), 1)
    }

    showNCards (n: number) {
        for (let i = 0; i < n; i++) {
            this.openCards.push(this.deck.draw())
        }
    }

    areBetsEqual () {
        let isEqual = true
        this.players.forEach(player => {
            if (player.getBet() !== this.biggestBet) {
                isEqual = false
            }
        })
        return isEqual
    }

    getOpenCards () {
        return this.openCards
    }

    checkWinner () {
        let biggest = Hands.HIGH_CARD
        let winner: Player
        this.players.forEach(player => {
            const cards = this.openCards.concat(player.getHand())
            const value = this.checkHand(cards)
            if (value > biggest) {
                biggest = value
                winner = player
            }
        })
        return winner
    }

    checkHand(cards: Card[]) {
        const { pairs, three, four } = this.checkPairs(cards)
        const isStraightFlush = this.checkStraightFlush(cards)
        const isFlush = this.checkFlush(cards)
        const isStraight = this.checkStraight(cards)

        if (isStraightFlush) {
            return Hands.STRAIGHT_FLUSH
        } else if (isStraight) {
            return Hands.STRAIGHT
        } else if (isFlush) {
            return Hands.FLUSH
        } else if (pairs === 1 && three === 0) {
            return Hands.PAIR
        } else if (pairs === 2) {
            return Hands.TWO_PAIR
        } else if (three === 2) {
            return Hands.THREE_OF_A_KIND
        } else if (pairs === 1 && three === 1) {
            return Hands.FULL_HOUSE
        } else if (four === 1) {
            return Hands.FOUR_OF_A_KIND
        } else {
            return Hands.HIGH_CARD
        }
    }

    checkPairs (cards: Card[]) {
        const dups = _.groupBy(cards, c => c.value)
        const groups = _.pickBy(dups, c => c.length > 1)

        let pairs = 0
        let three = 0
        let four = 0
        Object.keys(groups).forEach(group => {
            if (groups[group].length === 2) {
                pairs++
            } else if (groups[group].length === 3) {
                three++
            } else if (groups[group].length === 4) {
                four++
            }
        })

        return { pairs, three, four }
    }

    checkStraightFlush(cards: Card[]) {
        const group = _.groupBy(cards, c => c.suit)
        const straights = _.omitBy(group, suit => suit.length < 5)
        if (Object.keys(straights).length) {
            const sorted = _.sortBy(straights[Object.keys(straights)[0]], card => Number(card.value))
            let count = 0
            for (let i = 0; i < sorted.length - 1; i++) {
                if (Number(sorted[i].value) + 1 === Number(sorted[i + 1].value)) {
                    count++
                }
            }
            if (count >= 5) {
                return true
            }
        }
        return false
    }

    checkStraight (cards: Card[]) {
        const sorted = _.sortBy(cards, card => Number(card.value))
        let count = 0
        for (let i = 0; i < sorted.length - 1; i++) {
            if (Number(sorted[i].value) + 1 === Number(sorted[i + 1].value)) {
                count++
            }
        }
        if (count >= 5) {
            return true
        }
        return false
    }

    checkFlush (cards: Card[]) {
        const group = _.groupBy(cards, c => c.suit)
        const flushes = _.omitBy(group, suit => suit.length < 5)
        if (Object.keys(flushes).length) {
            return true
        }
        return false
    }

    getDealer() {
        return this.players[this.dealerPosition]
    }

    broadcastGameInfo() {
        this.players.forEach(player => {
            player.ws.send(JSON.stringify({ pot: this.pot, stack: player.getChips(), bet: player.getBet() }))
        })
    }

    async runGame () {
        if (this.table.getPlayers().length < 2) {
            console.error('A game needs at least 2 players to start')
            return
        }
    
        // main game loop
        while (this.table.getPlayers().length > 1) {
            const game = this.table.startGame()
            game.givePlayerCards()
            game.setPlayOrder(true)
            game.setBlinds()
            game.broadcastGameInfo()
            console.log(`Pot: ${game.getPot()}`)
    
            // show cards to players (and blinds)
            this.table.getPlayers().forEach(player => {
                player.ws.send(JSON.stringify({ hand: player.getHand(), cards: [] }), err => {
                    if (err) console.error(err)
                })
            })
    
            // pre-flop bets
            let hasBetOnce = false
            while (!game.areBetsEqual() || game.getPlayers().length === 1) {
                for (const player of game.getPlayers().slice()) {
                    if (player === game.getBiggestBetPlayer() || (hasBetOnce && player.getBet() === game.getBiggestBet()) || player.isAllIn()) {
                        continue
                    }
                    console.log('Player ', player.getName(), 'has', player.getChips())
        
                    // ask for move
                    const { move, value } = await player.getPlayerInput(game)
                    player.makeMove(move, value, game)
    
                    this.table.broadcastMessage(`Player ${player.getName()} chose: ${move} ${value}`)
                }
                hasBetOnce = true
                console.log('bets', game.getPlayers().map(p => ({bet: p.getBet(), name: p.getName()})), 'biggest', game.getBiggestBet())
                console.log('Dealer:', game.getDealer().getName(), 'Bet:', game.getDealer().getBet())
            }
            game.broadcastGameInfo()
    
            this.table.broadcastMessage({ message: `Game pot is ${game.getPot()}`, pot: game.getPot() })
    
            // flop, turn, river
            game.setPlayOrder(false)
            for (let i = 0; i < 3; i++) {
                console.log(game.getPlayers().map(p => p.getName()).join(', '))
                game.setBiggestBet(0)
                game.setBiggestBetPlayer(undefined)
                game.getPlayers().forEach(player => player.resetBet())
    
                game.showNCards(i === 0 ? 3 : 1)
    
                Deck.print(game.getOpenCards())
                this.table.getPlayers().forEach(player => {
                    player.ws.send(JSON.stringify({ cards: game.getOpenCards() }), err => {
                        if (err) console.error(err)
                    })
                })
    
                // bets
                let hasBetOnce = false
                do {
                    for (const player of game.getPlayers().slice()) {
                        if (player === game.getBiggestBetPlayer() || (hasBetOnce && player.getBet() === game.getBiggestBet()) || player.isAllIn()) {
                            continue
                        }
                        const { move, value } = await player.getPlayerInput(game)
                        player.makeMove(move, value, game)
                    }
                    hasBetOnce = true
                    console.log('bets', game.getPlayers().map(p => p.getBet()), 'biggest', game.getBiggestBet())
                } while (!game.areBetsEqual() || game.getPlayers().length === 1)
    
                game.broadcastGameInfo()
            }
    
            const winner = game.checkWinner()
            game.cashPot(winner)
            console.log(winner.getChips())
    
            this.table.broadcastMessage(`Winner is ${winner.getName()}`)
        }
    }
}

export enum Hands {
    HIGH_CARD = 1,
    PAIR,
    TWO_PAIR,
    THREE_OF_A_KIND,
    STRAIGHT,
    FLUSH,
    FULL_HOUSE,
    FOUR_OF_A_KIND,
    STRAIGHT_FLUSH,
    ROYAL_FLUSH
}