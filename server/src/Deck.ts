import * as _ from 'lodash'
import { indexOf } from 'lodash'

export class Deck {
    private cards: Card[]

    constructor () {
        this.cards = []
        this.initDeck()
        this.shuffle()
    }

    initDeck () {
        const suits = ['♠', '♥', '♣', '♦']
        const values = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
        const labels = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
        suits.forEach(suit => {
            values.forEach(value => {
                this.cards.push({ suit, value, label: labels[indexOf(values, value)] })
            })
        })
    }

    shuffle () {
        this.cards = _.shuffle(this.cards)
    }

    draw () {
        return this.cards.pop()
    }

    static print(cards: Card[]) {
        const printTop = cards.map(card => `${card.label}${card.label.length > 1 ? ' ': '  '}${card.suit}`)
        const printBottom = cards.map(card => `${card.suit}${card.label.length > 1 ? ' ': '  '}${card.label}`)
        console.log(cards.map(c => '┏━━━━┓').join('  '))
        console.log(`┃${printTop.join('┃  ┃')}┃`)
        console.log(cards.map(c => '┃    ┃').join('  '))
        console.log(`┃${printBottom.join('┃  ┃')}┃`)
        console.log(cards.map(c => '┗━━━━┛').join('  '))
    }

}

export interface Card {
    suit: string
    value: number
    label: string
}