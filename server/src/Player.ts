import { Socket } from "socket.io"
import { Card } from "./Deck"
import { Game } from "./Game"

export class Player {

    private name: string
    private stack: number
    private hand: Card[]
    private allIn: boolean
    private bet: number
    ws: Socket

    constructor (name: string, ws: Socket) {
        this.name = name
        this.ws = ws
        this.bet = 0
    }

    getName () {
        return this.name
    }

    setChips (value: number) {
        this.stack = value
    }

    getChips () {
        return this.stack
    }

    setHand (cards: Card[]) {
        this.hand = cards
    }

    getHand () {
        return this.hand
    }

    isAllIn () {
        return this.allIn
    }

    makeBet (value: number) {
        if (this.stack < value) {
            this.allIn = true
            this.stack = 0
            return
        }
        this.bet += value
        this.stack = this.stack - value
    }

    getBet () {
        return this.bet
    }

    resetBet () {
        this.bet = 0
    }

    async getResponse () {
        return new Promise<string>((resolve, reject) => {
            this.ws.on('message', messageBuffer => {
                resolve(messageBuffer.toString().trim())
            })
        })
    }

    async getPlayerInput (game: Game) {
        const moves = game.getPossibleMoves(this)
        const message = `Your move. Your options are: ${moves.join(',')}`
        this.ws.send(JSON.stringify({ message }))
        const response = await this.getResponse()
        console.log('Player move:', response)
        let [move, value] = response.split(' ')
        while (!moves.includes(move) || (move === 'raise' && value === undefined) || (move === 'raise' && Number(value) <= game.getBiggestBet())) {
            console.log('Invalid move:', move)
            this.ws.send(JSON.stringify({ message }))
            const response = await this.getResponse()
            console.log('Player move:', response)
            const arr = response.split(' ')
            move = arr[0]
            value = arr[1]
        }
        return { move, value: Number(value) || 0 }
    }

    makeMove(move: string, value: number, game: Game) {
        switch (move) {
            case 'check':
                // broadcast decision
                break
            case 'call':
                // broadcast decision
                console.log('biggest', game.getBiggestBet(), 'bet', this.getBet())
                const diffCall = game.getBiggestBet() - this.getBet()
                game.makeBet(this, diffCall)
                break
                case 'raise':
                console.log('value', value, 'bet', this.getBet())
                const diffRaise = value - this.getBet()
                game.makeBet(this, diffRaise)
                break
            case 'fold':
                game.removePlayer(this)
                break
        }
    }
}