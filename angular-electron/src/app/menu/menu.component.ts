import { Component, OnInit } from '@angular/core';
import { MenuService, Table } from './menu.service';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  tables: Table[] = []
  displayedColumns: string[] = ['select', 'id', 'name', 'players', 'join'];
  selection = new SelectionModel<Table>(true, []);

  constructor(private menuService: MenuService) { }

  ngOnInit() {
    this.updateTableList()
  }

  updateTableList() {
    this.menuService.listTables().subscribe(data => {
      this.tables = data
    })
  }

  createTable() {
    this.menuService.createTable().subscribe(data => {
      this.tables = data
    })
  }

  deleteSelected() {
    this.selection.selected.forEach(table => {
      this.menuService.deleteTable(table.id).subscribe(data => {
        this.tables = data
      })
    })
  }

  joinTable(row) {
    
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.tables.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.tables);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Table): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

}
