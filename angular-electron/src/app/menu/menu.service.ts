import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private http: HttpClient) { }

  listTables() {
    return this.http.get<Table[]>('http://localhost:7070/tables')
  }

  createTable() {
    return this.http.post<Table[]>('http://localhost:7070/tables', {})
  }

  deleteTable(id: string) {
    return this.http.delete<Table[]>(`http://localhost:7070/tables/${id}`)
  }
}

export interface Table {
  id: string
  name: string
}