# Instruções de execução
Para compilar e executar o projeto, basta clonar o repositório https://gitlab.com/caiosobreiro/websocket-poker ou utilizar o código fornecido no arquivo .zip. É recomendado utilizar o código do repositório para obter a versão mais recente.

### pré-requisitos:
Ter node.js e npm instalado: https://nodejs.org/en/download/ 


Antes de iniciar a execução é necessário instalar as dependências do cliente e do servidor. Para isso basta entrar no diretório client e executar o comando npm install. Em seguida é só executar o mesmo comando na pasta server.

Em seguida é necessário realizar o build (transpilação de Typescript para Javascript) com o comando npm run build em cada um dos diretórios acima.

Agora é só executar o servidor com o comando npm start e em seguida executar os clientes com o mesmo comando.

Em resumo:

cd server
npm install
npm run build
npm start

cd client
npm install
npm run build
npm start
