import WebSocket from 'ws'
import { UI } from './UI'

const ui = new UI()

let ws = new WebSocket('ws://localhost:7071', {
    perMessageDeflate: false
})

console.log('Client connected to server on port 7071')

ws.on('open', function open() {
    ui.resetScreen()
    ui.render({ message: 'Starting client...' , hand: [], cards: []})
    process.stdin.on('data', data => {
        ws.send(data.toString())
    })
})

ws.on('message', (messageBuffer) => {
    const message = JSON.parse(messageBuffer.toString())
    ui.render(message)
})

ws.on('close', () => {
    console.log('Connection closed')
    process.exit(0)
})

ws.on('error', (err) => {
    console.error(err)
})
