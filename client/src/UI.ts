export class UI {

    columns: number
    rows: number

    // game info
    hand: Card[] = [] // pocket cards
    cards: Card[] = [] // table cards
    tableId: string = ''
    numberOfPlayers: number = 0
    playerName: string = ''
    stack: number = 0
    pot: number = 0
    bet: number = 0
    messageHistory: string[] = ['', '', '', '', '']

    constructor () {
        this.columns = process.stdout.columns
        this.rows = process.stdout.rows
    }

    render (message: Message) {
        this.saveStateToObject(message)

        console.clear()
        const middleReplaced = this.replaceCharAt(this.generateHorizontalDivider(), 17, '┳')
        const endReplaced = this.replaceCharAt(middleReplaced, middleReplaced.length, '┓')
        const startReplaced = this.replaceCharAt(endReplaced, 0, '┏')
        console.log(startReplaced)

        this.printHand(this.hand, [
            this.playerName,
            this.tableId,
            this.numberOfPlayers.toString(),
            this.stack.toString(),
            this.pot.toString(),
            this.bet.toString()])

        const middleReplaced2 = this.replaceCharAt(this.generateHorizontalDivider(), 17, '┻')
        const startReplaced2 = this.replaceCharAt(middleReplaced2, 0, '┣')
        const endReplaced2 = this.replaceCharAt(startReplaced2, startReplaced2.length, '┫')
        console.log(endReplaced2)

        this.printTable(this.cards)

        console.log(this.replaceCharAt(endReplaced2, 17, '━'))

        const last5Messages = this.messageHistory.slice(-5)
        for (const message of last5Messages) {
            this.drawLineWithBorders(message)
        }

        const startReplaced3 = this.replaceCharAt(this.generateHorizontalDivider(), 0, '┗')
        const endReplaced3 = this.replaceCharAt(startReplaced3, startReplaced2.length, '┛')
        console.log(endReplaced3)

        process.stdout.write('Command: ')
    }

    printTable(cards: Card[]) {
        if (!cards.length) {
            for (let i = 0; i < 5; i++) {
                this.drawLineWithBorders('')
            }
            return
        }

        const printTop = cards.map(card => `${card.label}${card.label.length > 1 ? ' ': '  '}${card.suit}`)
        const printBottom = cards.map(card => `${card.suit}${card.label.length > 1 ? ' ': '  '}${card.label}`)

        const padStart = Math.round((this.columns - 2) / 2) + Math.round((cards.length * 6 + 2 * cards.length - 1) / 2)
        this.drawLineWithBorders(cards.map(c => '┏━━━━┓').join('  ').padStart(padStart))
        this.drawLineWithBorders(`┃${printTop.join('┃  ┃')}┃`.padStart(padStart))
        this.drawLineWithBorders(cards.map(c => '┃    ┃').join('  ').padStart(padStart))
        this.drawLineWithBorders(`┃${printBottom.join('┃  ┃')}┃`.padStart(padStart))
        this.drawLineWithBorders(cards.map(c => '┗━━━━┛').join('  ').padStart(padStart))
    }

    printHand(cards: Card[], info: string[]) {
        if (!this.hand.length) {
            for (let i = 0; i < 5; i++) {
                this.drawLineWithBorders('                ┃')
            }
            return
        }

        const printTop = cards.map(card => `${card.label}${card.label.length > 1 ? ' ': '  '}${card.suit}`)
        const printBottom = cards.map(card => `${card.suit}${card.label.length > 1 ? ' ': '  '}${card.label}`)

        const playerName = `Player: ${info[0]}`
        const tableId = `Table ID: ${info[1]}`
        const noOfPlayers = `Players Seated: ${info[2]}`
        const stackSize = `Stack: ${info[3]}`
        const pot = `Pot: ${info[4]}`
        const bet = `Bet: ${info[5]}`

        this.drawLineWithBorders(' ' + cards.map(c => '┏━━━━┓').join('  '),  '┃',  playerName.padStart(this.columns - 21))
        this.drawLineWithBorders(' ' + `┃${printTop.join('┃  ┃')}┃`,         '┃', tableId.padStart(this.columns - 21))
        this.drawLineWithBorders(' ' + cards.map(c => '┃    ┃').join('  '),  '┃', noOfPlayers.padStart(this.columns - 21))
        this.drawLineWithBorders(' ' + `┃${printBottom.join('┃  ┃')}┃`,      '┃', stackSize.padStart(this.columns - 21))
        this.drawLineWithBorders(' ' + cards.map(c => '┗━━━━┛').join('  '),  '┃', pot.padStart(this.columns - 21))
        this.drawLineWithBorders('   Your hand    ┃',                             bet.padStart(this.columns - 21))
    }

    resetScreen () {
        this.hand = undefined
    }

    replaceCharAt(str: string, index: number, char: string) {
        const tmpArr = str.split('')
        tmpArr[index] = char
        return tmpArr.join('')
    }

    drawLineWithBorders (...strs: string[]) {
        const str = strs.join(' ')
        const padded = str.padStart(str.length + 1).padEnd(this.columns - 1)
        const tmp = this.replaceCharAt(padded, 0, '┃')
        console.log(this.replaceCharAt(tmp, this.columns, '┃'))
    }

    generateHorizontalDivider () {
        let str = ''
        for (let i = 0; i < this.columns - 1; i++) {
            str += '━'
        }
        return str
    }

    setPlayerName (value: string) {
        this.playerName = value
    }

    saveStateToObject (message: Message) {
        if (message.hand) {
            this.hand = message.hand
        }

        if (message.message) {
            this.messageHistory.push(message.message)
        }

        if (message.cards) {
            this.cards = message.cards
        }

        if (message.pot) {
            this.pot = message.pot
        }

        if (message.tableId) {
            this.tableId = message.tableId
        }

        if (message.playerName) {
            this.playerName = message.playerName
        }

        if (message.numberOfPlayers) {
            this.numberOfPlayers = message.numberOfPlayers
        }

        if (message.stack) {
            this.stack = message.stack
        }

        if (message.bet) {
            this.bet = message.bet
        }
    }
}

export interface Message {
    hand?: Card[],
    message?: string,
    cards?: Card[],
    tableId?: string,
    playerName?: string,
    pot?: number,
    stack?: number,
    bet?: number,
    numberOfPlayers?: number
}

export interface Card {
    suit: string
    value: number
    label: string
}